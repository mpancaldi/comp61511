
import sys

def miniwc():
    args_len = len(sys.argv)
    if args_len == 2:
        file = sys.argv[1]
    elif args_len > 2:
        print("Only one file as argument.")
        exit()
    else:
        print("No file argument was inserted.")
        exit()

    byte_count = 0
    word_count = 0
    line_count = 0

    try:
        with open(file, 'r', encoding="utf8") as f:
            for line in f:
                byte_count += len(line.encode("utf8"))
                if "\n" in line:    # wc counts "\n" as lines
                    line_count += 1

                wordsep = '\t\n\x0b\x0c\r\x1c\x1d\x1e\x1f \x85\xa0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006' \
                          '\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000 '
                target = ' ' * len(wordsep)
                trans = ''.maketrans(wordsep, target)
                line = line.translate(trans)
                words = line.split()
                word_count += len(words)

            print("\t" + str(line_count) + "\t" + str(word_count) + "\t" + str(byte_count) + "\t" + file)

    except FileNotFoundError:
        print("wc: " + file + ": open: No such file or directory")
    except UnicodeDecodeError:
        #TODO: fix this error msg
        print("Error: cannot read file \"" + file + "\" correctly.")

miniwc()
