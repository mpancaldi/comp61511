"""
>>> import subprocess

>>> def test(input):
...     return subprocess.check_output("python3 miniwc31.py " + input, shell=True)

>>> test('')
'No file argument was inserted.\\n'
>>> test(' ')
'No file argument was inserted.\\n'

>>> test('-')
'wc: -: No such file or directory\\n'
>>> test('@')
'wc: @: No such file or directory\\n'
>>> test('noSuchFile')
'wc: noSuchFile: No such file or directory\\n'

>>> test('file1 file2')
'Only one file as argument.\\n'
>>> test('file1 -')
'Only one file as argument.\\n'
>>> test('- -')
'Only one file as argument.\\n'
>>> test('-w file')
'Only one file as argument.\\n'
>>> test('file1 file2 file3')
'Only one file as argument.\\n'

>>> test('test_files/100B_file.txt')
'\\t0\\t1\\t100\\ttest_files/100B_file.txt\\n'


"""

if __name__ =="__main__":
   import doctest
   doctest.testmod()

# format("6 5 43 t.txt") returns "\\t6\\t5\\t43\\tt.txt\\n"

#python -m doctest -v doctest_wc.py
