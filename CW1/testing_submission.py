
import subprocess
import os
import re
import zipfile as ziplib

TEST_FILES_DIR = 'test_files/'
SCRIPT = 'miniwc31.py'
ALLOWED_MODULES = ('sys', 'os')
ZIP_FILE = 'mbaxtmp2_cw1.zip'


# TODO: student's and teacher's test files


def compare_outputs_miniwc():

    file_list = get_files_in_dir(TEST_FILES_DIR)
    results = []

    for file in file_list:

        miniwc_output = get_console_output(file)
        wc_output = get_wc_output(file)

        print("miniwc:\t" + miniwc_output)
        print("wc: \t\t" + wc_output)

        miniwc = miniwc_output.split()
        wc = wc_output.split()

        if len(miniwc) != len(wc):
            print('Some output value missing')
        else:
            results.append(compare_miniwc_and_wc(miniwc, wc))
    score = 0
    for b in results:
        if b:
            score += 1
    print("Output was correct for " + str(score) + " out of " + str(len(file_list)) + " files.")


def check_formatting():
    # the output needs to be on a single line, have 3 numbers followed by the file name,
    # plus the number must be in the order 'lines words chars'
    file = get_files_in_dir(TEST_FILES_DIR)[0]
    output = get_console_output(file).split()
    wc_output = get_wc_output(file).split()

    compare = compare_miniwc_and_wc(output, wc_output)


def get_files_in_dir(dir_name):
    return os.listdir(dir_name)


def get_console_output(file):
    return subprocess.check_output(['python3', SCRIPT, TEST_FILES_DIR + file]).decode('ascii')


def get_wc_output(file):
    return subprocess.check_output(['wc', TEST_FILES_DIR + file]).decode('ascii')


def compare_miniwc_and_wc(miniwc, wc):
    compare = [miniwc[0] == wc[0], miniwc[1] == wc[1], miniwc[2] == wc[2], miniwc[3] == wc[3]]
    return compare[0] & compare[1] & compare[2] & compare[3]


def check_imports(script):
    try:
        with open(script, "r") as script:
            matches = []
            for line in script:
                m1 = re.match('import (\w+)', line)
                m2 = re.match('from (\w+) import', line)
                if m1:
                    matches.append(m1.group(1))
                if m2:
                    matches.append(m2.group(1))
            if len( list(set(matches) - set(ALLOWED_MODULES))) > 0:
                print("Prohibited libraries have been used.")
                return "Prohibited libraries"
            else:
                print ("Only " + str(ALLOWED_MODULES) + " have been used.")
                return "Allowed libraries"
            print (matches)
    except IOError:
        print("The selected script was not found (wrong file name?)")
        raise IOError


def unzip_and_check_submission_content(zip_file):
    assignment_dir = zip_file[:-4]
    try:
        with ziplib.ZipFile(zip_file, "r") as zip_ref:
            zip_ref.extractall()
        files = get_files_in_dir(assignment_dir)
        required_files = ['miniwc.py', 'doctest_miniwc.py', 'test_files']
        if all(i in files for i in required_files):
            print("The submission contains all the required files.")
            return "Files ok"
        else:
            print("The following files are missing: " + str( list(set(required_files) - set(files)) ))
            return "Missing files"

    except IOError as e:
        print( str(e) )
        raise IOError
    except ziplib.BadZipfile as zip_e:
        print (str(zip_e))
        raise ziplib.BadZipfile




#compare_outputs_miniwc()
#check_imports(SCRIPT)

unzip_and_check_submission_content(ZIP_FILE)