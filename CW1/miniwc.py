
import sys
import re

def miniwc():
    args_len = len(sys.argv)
    if args_len == 2:
        file = sys.argv[1]
    elif args_len > 2:
        print("Only one file as argument.")
        exit()
    else:
        print("No file argument was inserted.")
        exit()

    byte_count = 0
    word_count = 0
    line_count = 0

    try:
        with open(file, 'r+', encoding='utf8') as f:
            for line in f:
                # #words3 = re.compile("(\s)").split(line)
                # if ('\u2005\u2005' in line) | ('\u2005' in line):
                #     words = re.compile("(\s)").split(line)
                # #else ('\u2005' in line):
                #  #   words = re.split(' |\u2005|\t', line)
                #     #words = re.compile("(\s)").split('\u2005', line)
                # else:
                #     words = re.split(' |\u2005|\t', line)
                #
                # words2 = line.split()  # miniwc doesn't recognise unicode spaces as spaces
                # if "\n" in line:
                #     line_count += 1
                #
                # #words.remove('').remove('\n')
                # words = [x for x in words if x != '\n']
                # words = [x for x in words if x != ""]
                # words = [x for x in words if x != '\t']
                # words = [x for x in words if x != ' ']

                words = line.split('\u2005')
                words += line.split(' ')
                if "\n" in line:
                    line_count += 1;
                    words = [x for x in words if x != '\n'] 
                word_count += len(words)
                byte_count += len(line.encode("utf8"))
            print("\t" +str(line_count) + "\t" + str(word_count) + "\t" + str(byte_count) + "\t" + file)
    except FileNotFoundError:
        print("wc: " + file + ": open: No such file or directory")
    except UnicodeDecodeError:
        #TODO: fix this error msg
        print("Error: cannot read file \"" + file + "\" correctly.")

miniwc()
