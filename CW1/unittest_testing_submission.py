import unittest
import zipfile
from testing_submission import *

class Test(unittest.TestCase):

    def test_check_import(self):
        self.assertEqual(check_imports("miniwc31.py"), "Allowed libraries")
        self.assertEqual(check_imports("miniwc4.py"), "Allowed libraries")
        self.assertRaises(IOError, check_imports, "")
        self.assertRaises(IOError, check_imports, "scriptnotfound.py")
        self.assertEqual(check_imports("testing_submission.py"), "Prohibited libraries")
        self.assertEqual(check_imports("unittest_testing_submission.py"), "Prohibited libraries")

    def test_unzip_and_check_submission_content(self):
        self.assertEqual(unzip_and_check_submission_content("mbaxtmp2_cw1.zip"), "Files ok")
        self.assertEqual(unzip_and_check_submission_content("test.zip"), "Missing files")
        self.assertRaises(IOError, unzip_and_check_submission_content, "nosuch.zip")
        self.assertRaises(IOError, unzip_and_check_submission_content, "mbaxtmp2_cw1")
        self.assertRaises(IOError, unzip_and_check_submission_content, "")
        self.assertRaises(zipfile.BadZipfile, unzip_and_check_submission_content, "miniwc.py")


if __name__ == '__main__':
    unittest.main()