
import sys
import os

def miniwc():
    args_len = len(sys.argv)
    if args_len == 2:
        file = sys.argv[1]
    elif args_len > 2:
        print("Only one file as argument.")
        exit()
    else:
        print("No file argument was inserted.")
        exit()

    byte_count = os.stat(file).st_size
    word_count = 0
    line_count = 0

    try:
        with open(file, "rb") as bookFile:
            text = bookFile.read()
            utf8 = text.decode('latin-1')
            wordList = utf8.splitlines()

            for c in utf8:
                 if c == '\n':
                     line_count += 1
                 #elif c == ' ':
                 #    word_count += 1

            textlines = text.splitlines()

            for line in textlines:
                trans = ''.maketrans('\x0b', '-')
                utfline = line.decode('latin1')
                utfline = utfline.translate(trans)
                words = utfline.split()
                words = [x for x in words if x != b'']
                word_count += len(words)

            #wordsep = '\t\n\x0b\x0c\r\x1c\x1d\x1e\x1f\x85\xa0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006' \
             #         '\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000 '
            #target = ' ' * len(wordsep)
            #trans = ''.maketrans(wordsep, target)
            #utf8 = utf8.translate(trans)


            #words = utf8.split()
            #words = [x for x in words if x != ""]
            #words = [x for x in words if x != "\n"]
            #word_count = len(words)

            # for line in wordList:
            #     #if '\n' in line:
            #     #    line_count += 1
            #
            #     words = line.split()
            #     words = [x for x in words if x != ""]
            #     words = [x for x in words if x != "\n"]
            #     #word_count += len(words)

        print("\t" + str(line_count) + "\t" + str(word_count) + "\t" + str(byte_count) + "\t" + file)

    except FileNotFoundError:
        print("wc: " + file + ": open: No such file or directory")

    # try:
    #     with open(file, 'rb') as f:
    #         for line in f:
    #             #byte_count += len(line.encode("utf8"))
    #             #str = line.encode('utf-8')
    #             #if '\n' in line.decode('ascii'):    # wc counts "\n" as lines
    #             #line_count += 1
    #             #string = line.decode()
    #             #wordsep = '\t\n\x0b\x0c\r\x1c\x1d\x1e\x1f \x85\xa0\u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006' \
    #              #         '\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000 '
    #             #target = ' ' * len(wordsep)
    #             #trans = ''.maketrans(wordsep, target)
    #             #line = line.translate(trans)
    #             words = line.split()
    #             #word_count += len(words)
    #
    #         #print("\t" + str(line_count) + "\t" + str(word_count) + "\t" + str(byte_count) + "\t" + file)
    #
    # except FileNotFoundError:
    #     print("wc: " + file + ": open: No such file or directory")
    # except UnicodeDecodeError:
    #     #TODO: fix this error msg
    #     print("Error: cannot read file \"" + file + "\" correctly.")




miniwc()
