import ast

source = '6 + 8 * 3'
node = ast.parse(source, mode='eval')

print(eval(compile(node, '<string>', mode='eval')))

print(ast.dump(node) + "\n")


node = ast.Expression(
                ast.BinOp(
                    ast.Str('xy'),
                    ast.Mult(),
                    ast.Num(4)
                )
            )

fixed = ast.fix_missing_locations(node)

codeobj = compile(fixed, '<string>', 'eval')
print(eval(codeobj))
print(ast.dump(node) + "\n")

source = "6 + 8"
node = ast.parse(source, mode='eval')
print(eval(compile(node, '<string>', mode='eval')))
print(ast.dump(node) + "\n")

original_code='''
my_list=['raindrops n roses', 'whiskers on kittens']
my_list.append('bright copper kettles')
'''

AST(original_code).find('Call')
