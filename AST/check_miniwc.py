# from modulefinder import ModuleFinder
#
# finder = ModuleFinder()
# finder.run_script("miniwc.py")
# for name, mod in finder.modules.iteritems():
#     print(name)

import sys
import re
import unittest
import argparse


modulenames = set(sys.modules)&set(globals())
print modulenames


from modulefinder import ModuleFinder

finder = ModuleFinder()
finder.run_script('miniwc.py')

print 'Loaded modules:'
for name, mod in finder.modules.iteritems():
    print '%s ' % name,

print '-'*50
modulenames = set(finder.modules.iteritems())&set(globals())
print set(globals())
print '-'*50
print set(finder.modules)
print '-'*50

from modulefinder import ModuleFinder

finder = ModuleFinder()
finder.run_script('miniwc.py')

print 'Loaded modules:'
for name, mod in finder.modules.iteritems():
    print '%s: ' % name,
    print ','.join(mod.globalnames.keys()[:3])

print '-'*50


finder = ModuleFinder()
finder.run_script('miniwc.py')

moduleslist = {}
for name, mod in finder.modules.iteritems():
    filename = mod.__file__
    if filename is None:
        continue
    if '__' in name:
        continue
    #if "python" in filename.lower():
    #    continue
    moduleslist[name.split(".")[0]] = True
    #print '%s: %s' % (name, filename)
    #print ','.join(mod.globalnames.keys()[:3])

print 'Loaded modules:'
for name, dummy in moduleslist.iteritems():
    print(name)
